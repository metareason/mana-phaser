@Component('cubePhysics')
export class CubePhysics {
  velocity: Vector3 
  acceleration: Vector3 
  constructor(velocity: Vector3, acceleration: Vector3) {
    this.velocity = velocity
    this.acceleration = acceleration
  }
}

class FallSystem {
  group = engine.getComponentGroup(Transform)

  update(dt: number) {
    // iterate over the entities of the group
    for (let entity of this.group.entities) {
      // get the Transform component of the entity
      const transform = entity.getComponent(Transform)
      transform.translate(new Vector3(0,.1 * dt,0))
      // mutate the rotation
      // transform.rotate(Vector3.Up(), dt * 10)
    }
  }
}

class CubeSystem {
  // this group will contain every entity that has a Transform component
  group = engine.getComponentGroup(CubePhysics)

  update(dt: number) {
    // iterate over the entities of the group
    for (let entity of this.group.entities) {
      // get the Transform component of the entity
      const cubePhysics = entity.getComponent(CubePhysics)
      // get the Transform component of the entity
      const transform = entity.getComponent(Transform)
      if (transform.position.y < 1) {
        transform.position = new Vector3(transform.position.x, 19, transform.position.z)
      }
      transform.translate(new Vector3(0, -3 * dt, 0))
    }
  }
}

// Add a new instance of the system to the engine
// engine.addSystem(new RotatorSystem())
// engine.addSystem(new FallSystem())
engine.addSystem(new CubeSystem())


function setupPhaseMachine(x: number, y: number, z: number) {
  // create the entity
  const phaseMachine = new Entity()

  // add a transform to the entity
  phaseMachine.addComponent(new Transform({ position: new Vector3(x, y, z) }))

  // phaseMachine.addComponent(new GLTFShape("models/agora-space-03.gltf"))
  phaseMachine.addComponent(new GLTFShape("models/dlh2-machine-02.gltf"))

  // add the entity to the engine
  engine.addEntity(phaseMachine)

  return phaseMachine
}

function dropPhaseCube(x: number, y: number, z: number) {
  const phaseCube = new Entity()

  phaseCube.addComponent(new Transform({ position: new Vector3(x, y, z) }))
  phaseCube.addComponent(new CubePhysics(new Vector3(x, y, z), new Vector3(x, y, z)))
  phaseCube.addComponent(
    new OnClick(() => {
      engine.removeEntity(phaseCube)
    })
  )

  // add a shape to the entity
  // phaseCube.addComponent(new BoxShape())

  phaseCube.addComponent(new GLTFShape("models/texture-cube.gltf"))

  // add the entity to the engine
  engine.addEntity(phaseCube)

  return phaseCube
}

const phaseMachine = setupPhaseMachine(8,0,8)
for (let c = 0; c < 10; c++) {
  dropPhaseCube(Math.random() * 6 + 4, Math.random() * 16, Math.random() * 6 + 4)
}

const canvas = new UICanvas()
let imageAtlas = "images/mana-phaser-ui.png"
let imageTexture = new Texture(imageAtlas)
const manaPhaserLogo = new UIImage(canvas, imageTexture)
manaPhaserLogo.sourceLeft = 1
manaPhaserLogo.sourceTop = 1
manaPhaserLogo.sourceWidth = 512
manaPhaserLogo.sourceHeight = 200
manaPhaserLogo.vAlign = 'top'
manaPhaserLogo.hAlign = 'left'
manaPhaserLogo.paddingLeft = 15
manaPhaserLogo.width = 256
manaPhaserLogo.height = 100
